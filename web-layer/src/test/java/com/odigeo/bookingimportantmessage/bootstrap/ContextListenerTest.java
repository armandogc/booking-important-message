package com.odigeo.bookingimportantmessage.bootstrap;

import org.apache.log4j.Logger;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class ContextListenerTest {

//    @Mock
//    private ServletContextEvent servletContextEvent;
//    @Mock
//    private ServletContext servletContext;
//    @Mock
//    private ContextListener contextListener;
//    @Mock
//    private Logger logger;
//
//
//    @BeforeMethod
//    public void setUp() {
//        initMocks(this);
//        when(servletContextEvent.getServletContext()).thenReturn(servletContext);
//        doCallRealMethod().when(contextListener).startUpApplication(servletContextEvent);
//        doCallRealMethod().when(contextListener).endApplication(servletContextEvent);
//        doNothing().when(contextListener).initConfigurationEngine(any());
//    }
//
//    @Test
//    public void startUpApplicationOK() {
//        when(contextListener.initLog4J(servletContext)).thenReturn(logger);
//        doNothing().when(logger).info(anyString());
//        doNothing().when(contextListener).initMetrics();
//        contextListener.startUpApplication(servletContextEvent);
//        verify(servletContext).log(anyString());
//        verify(contextListener).initLog4J(servletContext);
//        verify(logger).info(anyString());
//    }
//
//    @Test
//    public void createInstanceOfContextListenerOK() {
//        ContextListener contextListener = new ContextListener();
//        assertNotNull(contextListener);
//    }
//
//    @Test(expectedExceptions = IllegalStateException.class)
//    public void getAppNameThrowsIllegalStateException() {
//        doCallRealMethod().when(contextListener).getAppName();
//        doCallRealMethod().when(contextListener).initMetrics();
//        when(contextListener.initLog4J(servletContext)).thenReturn(logger);
//        contextListener.startUpApplication(servletContextEvent);
//    }
//
//
//    @Test
//    public void endApplicationOK() {
//        when(contextListener.initLog4J(servletContext)).thenReturn(logger);
//        contextListener.startUpApplication(servletContextEvent);
//
//        when(servletContextEvent.getServletContext()).thenReturn(servletContext);
//        contextListener.endApplication(servletContextEvent);
//        verify(servletContext, times(2)).log(anyString());
//    }

}
