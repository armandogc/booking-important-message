package com.odigeo.bookingimportantmessage.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingimportantmessage.BookingImportantMessageService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class ServiceApplicationTest {

    public static final int NUMBER_OF_CONTRACT_IMPLEMENTATIONS_AND_PROVIDERS_APP_USES = 2;
    @Mock
    private BookingImportantMessageService bookingImportantMessageService;

    private ServiceApplication serviceApplication;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init((Binder binder) -> {
            binder.bind(BookingImportantMessageService.class).toInstance(bookingImportantMessageService);
        });
        serviceApplication = new ServiceApplication();
    }

    @Test
    public void initServiceApplicationTest() {
        assertEquals(serviceApplication.getSingletons().size(), NUMBER_OF_CONTRACT_IMPLEMENTATIONS_AND_PROVIDERS_APP_USES);
    }

}
