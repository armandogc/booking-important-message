package com.odigeo.bookingimportantmessage.service;

import com.odigeo.bookingimportantmessage.BookingImportantMessageService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class BookingImportantMessageControllerTest {

    @Mock
    private BookingImportantMessageService bookingImportantMessageService;

    private BookingImportantMessageController bookingImportantMessageController;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        bookingImportantMessageController = new BookingImportantMessageController(bookingImportantMessageService);
    }

    @Test
    public void getCancellationTest()  {
        when(bookingImportantMessageService.getMessagesByBookingId(any())).thenReturn("Response");
        assertNotNull(bookingImportantMessageController.getImportantMessagesByBookingId(1L));
    }
}
