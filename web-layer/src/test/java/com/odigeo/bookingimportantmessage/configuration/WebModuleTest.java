package com.odigeo.bookingimportantmessage.configuration;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class WebModuleTest {

    @Test
    public void dummyTestToCoverConfigurationMethod() {
        WebModule webModule = new WebModule();
        ConfigurationEngine.init(webModule);
        assertNotNull(webModule);
    }


}
