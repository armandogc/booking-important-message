package com.odigeo.bookingimportantmessage.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingimportantmessage.BookingImportantMessageService;

import javax.ws.rs.core.Response;

@Singleton
public class BookingImportantMessageController implements com.odigeo.bookingimportantmessage.api.contract.v1.BookingImportantMessageService {

    private final BookingImportantMessageService bookingImportantMessageService;

    @Inject
    public BookingImportantMessageController(BookingImportantMessageService bookingImportantMessageService) {
        this.bookingImportantMessageService = bookingImportantMessageService;
    }

    @Override
    public Response getImportantMessagesByBookingId(Long bookingId) {
        return Response.ok(bookingImportantMessageService.getMessagesByBookingId(bookingId)).build();
    }
}
