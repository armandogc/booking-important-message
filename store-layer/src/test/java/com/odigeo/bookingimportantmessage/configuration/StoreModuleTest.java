package com.odigeo.bookingimportantmessage.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import com.odigeo.bookingimportantmessage.cancellation.CancellationApiStore;
import com.odigeo.bookingimportantmessage.flightincident.FlightIncidentStore;
import com.odigeo.bookingimportantmessage.travelcompanion.TravelCompanionStore;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class StoreModuleTest {

    @Mock
    private FlightIncidentStore flightIncidentStore;

    @Mock
    private CancellationApiStore cancellationApiStore;

    @Mock
    private TravelCompanionStore travelCompanionStore;

    private final StoreModule storeModule = new StoreModule();

    @BeforeTest
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void dummyTestToCoverJacocoCoverage() {
        assertNotNull(storeModule);
    }

    @Test
    public void dummyTestToCoverConfigurationMethod() {
        Guice.createInjector(Modules.override(new StoreModule()).with(new DummyModule()));
    }

    public class DummyModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(FlightIncidentStore.class).toInstance(flightIncidentStore);
            bind(CancellationApiStore.class).toInstance(cancellationApiStore);
            bind(TravelCompanionStore.class).toInstance(travelCompanionStore);
        }
    }

}
