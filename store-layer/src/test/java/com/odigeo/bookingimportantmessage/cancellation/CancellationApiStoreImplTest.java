package com.odigeo.bookingimportantmessage.cancellation;

import com.edreamsodigeo.customersupport.cancellation.v2.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v2.ProviderItemManualTaskResource;
import com.edreamsodigeo.customersupport.cancellation.v2.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v2.exception.ProviderItemManualTaskNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemManualTask;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class CancellationApiStoreImplTest {

    @Mock
    private CancellationResource cancellationResource;

    @Mock
    private ProviderItemManualTaskResource providerItemManualTaskResource;

    private CancellationApiStore cancellationApiStore;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        cancellationApiStore = new CancellationApiStoreImpl(cancellationResource, providerItemManualTaskResource);
    }

    @Test
    public void getCancellationTest() throws CancellationNotFoundException {
        when(cancellationResource.getCancellationsByIncident(any())).thenReturn(new ArrayList<>());
        assertNotNull(cancellationApiStore.getCancellations(UUID.randomUUID(), null));
    }

    @Test
    public void getProviderItemManualTaskTest() throws ProviderItemManualTaskNotFoundException {
        when(providerItemManualTaskResource.getProviderItemManualTaskByProviderItemId(any())).thenReturn(new ProviderItemManualTask());
        assertNotNull(cancellationApiStore.getCancellations(UUID.randomUUID(), null));
    }

}
