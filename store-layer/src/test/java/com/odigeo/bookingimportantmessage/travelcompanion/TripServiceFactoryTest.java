package com.odigeo.bookingimportantmessage.travelcompanion;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.travelcompanion.v2.TripService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TripServiceFactoryTest {

    private static final String URL = "http://localhost:8080";

    @Mock
    private TravelCompanionConfiguration travelCompanionConfiguration;

    private TripServiceFactory tripServiceFactory;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(TravelCompanionConfiguration.class).toProvider(() -> travelCompanionConfiguration));
        when(travelCompanionConfiguration.getUrl()).thenReturn(URL);
        tripServiceFactory = new TripServiceFactory(travelCompanionConfiguration);
    }

    @Test
    public void shouldCreateTripServiceWhenGetTripServiceIsCalled() {
        assertNotNull(tripServiceFactory.getTripService());
    }

    @Test
    public void shouldReturnSameTripServiceWhenGetTripServiceIsCalledTwiceWithSameUrl() {
        TripService tripService = tripServiceFactory.getTripService();
        assertEquals(tripService.hashCode(), tripServiceFactory.getTripService().hashCode());
    }
}
