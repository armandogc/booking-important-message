package com.odigeo.bookingimportantmessage.travelcompanion;

import com.odigeo.travelcompanion.v2.TripService;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class TravelCompanionStoreImplTest {

    @Mock
    private TripServiceFactory tripServiceFactory;

    @Mock
    private TripService tripService;

    private TravelCompanionStore travelCompanionStore;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        when(tripServiceFactory.getTripService()).thenReturn(tripService);
        travelCompanionStore = new TravelCompanionStoreImpl(tripServiceFactory);
    }

    @Test
    public void getCancellationTest() {
        when(tripService.getTrip(any(), any(), any(), any(), any())).thenReturn(new BookingDetail());
        assertNotNull(travelCompanionStore.getBooking(1L));
    }

}
