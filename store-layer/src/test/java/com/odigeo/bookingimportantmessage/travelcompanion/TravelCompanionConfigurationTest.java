package com.odigeo.bookingimportantmessage.travelcompanion;

import bean.test.BeanTest;

public class TravelCompanionConfigurationTest extends BeanTest<TravelCompanionConfiguration> {

    @Override
    protected TravelCompanionConfiguration getBean() {
        return new TravelCompanionConfiguration();
    }
}
