package com.odigeo.bookingimportantmessage.flightincident;

import com.edreamsodigeo.customersupport.flightincident.v1.FlightIncidentResource;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class FlightIncidentStoreImplTest {

    @Mock
    private FlightIncidentResource flightIncidentResource;

    private FlightIncidentStore flightIncidentStore;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        flightIncidentStore = new FlightIncidentStoreImpl(flightIncidentResource);
    }

    @Test
    public void getIncidentsTest() throws IncidentNotFoundException {
        when(flightIncidentResource.getIncidents(any())).thenReturn(new ArrayList<>());
        assertNotNull(flightIncidentStore.getIncidents(1L, null));
    }
}
