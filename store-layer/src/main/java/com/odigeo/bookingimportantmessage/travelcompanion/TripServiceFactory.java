package com.odigeo.bookingimportantmessage.travelcompanion;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingimportantmessage.configuration.ServicesFactory;
import com.odigeo.travelcompanion.v2.TripService;

import javax.annotation.concurrent.Immutable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@Immutable
public class TripServiceFactory extends ServicesFactory {

    private final Map<String, TripService> tripServiceMapByUrl = new ConcurrentHashMap<>();
    private final String url;

    @Inject
    public TripServiceFactory(final TravelCompanionConfiguration travelCompanionConfiguration) {
        this.url = travelCompanionConfiguration.getUrl();
    }

    public TripService getTripService() {
        TripService service = tripServiceMapByUrl.get(url);
        if (service == null) {
            service = buildService(TripService.class, url);
            tripServiceMapByUrl.put(url, service);
        }
        return service;
    }

}
