package com.odigeo.bookingimportantmessage.travelcompanion;

import com.google.inject.Inject;
import com.odigeo.travelcompanion.v2.TripService;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import com.odigeo.travelcompanion.v2.model.booking.TripDetailsMode;

public class TravelCompanionStoreImpl implements TravelCompanionStore {

    private final TripService tripService;

    @Inject
    public TravelCompanionStoreImpl(final TripServiceFactory tripServiceFactory) {
        this.tripService = tripServiceFactory.getTripService();
    }

    @Override
    public BookingDetail getBooking(Long bookingId) {
        return tripService.getTrip(bookingId, null, null, null, TripDetailsMode.ENRICHED);
    }
}
