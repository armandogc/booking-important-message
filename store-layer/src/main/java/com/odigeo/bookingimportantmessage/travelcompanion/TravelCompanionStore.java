package com.odigeo.bookingimportantmessage.travelcompanion;

import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;

public interface TravelCompanionStore {

    BookingDetail getBooking(Long bookingId);
}
