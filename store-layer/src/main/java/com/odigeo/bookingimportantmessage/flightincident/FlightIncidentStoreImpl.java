package com.odigeo.bookingimportantmessage.flightincident;

import com.edreamsodigeo.customersupport.flightincident.v1.FlightIncidentResource;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import com.edreamsodigeo.customersupport.flightincident.v1.model.BookingId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.google.inject.Inject;

import java.util.List;

public class FlightIncidentStoreImpl implements FlightIncidentStore {

    private final FlightIncidentResource flightIncidentResource;

    @Inject
    public FlightIncidentStoreImpl(final FlightIncidentResource flightIncidentResource) {
        this.flightIncidentResource = flightIncidentResource;
    }

    @Override
    public List<Incident> getIncidents(Long bookingId, Long providerBookingItemId) {
        try {
            return flightIncidentResource.getIncidents(new BookingId(String.valueOf(bookingId)));
        } catch (IncidentNotFoundException e) {
            return null;
        }
    }
}
