package com.odigeo.bookingimportantmessage.flightincident;

import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;

import java.util.List;

public interface FlightIncidentStore {

    List<Incident> getIncidents(Long bookingId, Long providerBookingItemId);

}
