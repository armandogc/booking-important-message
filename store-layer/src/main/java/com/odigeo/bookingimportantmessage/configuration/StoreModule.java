package com.odigeo.bookingimportantmessage.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.bookingimportantmessage.cancellation.CancellationApiStore;
import com.odigeo.bookingimportantmessage.cancellation.CancellationApiStoreImpl;
import com.odigeo.bookingimportantmessage.flightincident.FlightIncidentStore;
import com.odigeo.bookingimportantmessage.flightincident.FlightIncidentStoreImpl;
import com.odigeo.bookingimportantmessage.travelcompanion.TravelCompanionStore;
import com.odigeo.bookingimportantmessage.travelcompanion.TravelCompanionStoreImpl;

public class StoreModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(FlightIncidentStore.class).to(FlightIncidentStoreImpl.class);
        bind(CancellationApiStore.class).to(CancellationApiStoreImpl.class);
        bind(TravelCompanionStore.class).to(TravelCompanionStoreImpl.class);
    }

}
