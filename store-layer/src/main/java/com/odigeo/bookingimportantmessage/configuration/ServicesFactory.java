package com.odigeo.bookingimportantmessage.configuration;

import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;

public abstract class ServicesFactory {

    protected static final int CONNECTION_TIME_OUT = 20000;
    protected static final int SOCKET_TIME_OUT = 20000;
    protected static final int POOL_SIZE = 10;

    protected <T> T buildService(Class<T> serviceClass, String baseURL) {
        ServiceBuilder<T> serviceBuilder = new ServiceBuilder(serviceClass, baseURL, new SimpleRestErrorsHandler(serviceClass))
                .withConnectionConfiguration(getConfiguration());
        return serviceBuilder.build();
    }

    private ConnectionConfiguration getConfiguration() {
        return new ConnectionConfiguration.Builder()
                .maxConcurrentConnections(POOL_SIZE)
                .connectionTimeoutInMillis(CONNECTION_TIME_OUT)
                .socketTimeoutInMillis(SOCKET_TIME_OUT).build();
    }
}
