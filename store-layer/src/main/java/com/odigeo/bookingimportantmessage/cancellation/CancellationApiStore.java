package com.odigeo.bookingimportantmessage.cancellation;

import com.edreamsodigeo.customersupport.cancellation.v2.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemManualTask;

import java.util.List;
import java.util.UUID;

public interface CancellationApiStore {

    List<Cancellation> getCancellations(UUID incidentId, Long providerBookingId);

    ProviderItemManualTask getProviderItemManualTask(UUID providerItemId);
}
