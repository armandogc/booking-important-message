package com.odigeo.bookingimportantmessage.cancellation;

import com.edreamsodigeo.customersupport.cancellation.v2.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v2.ProviderItemManualTaskResource;
import com.edreamsodigeo.customersupport.cancellation.v2.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v2.exception.ProviderItemManualTaskNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v2.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v2.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemManualTask;
import com.google.inject.Inject;

import java.util.List;
import java.util.UUID;

public class CancellationApiStoreImpl implements CancellationApiStore {

    private final CancellationResource cancellationResource;
    private final ProviderItemManualTaskResource providerItemManualTaskResource;

    @Inject
    public CancellationApiStoreImpl(final CancellationResource cancellationResource,
                                    final ProviderItemManualTaskResource providerItemManualTaskResource) {
        this.cancellationResource = cancellationResource;
        this.providerItemManualTaskResource = providerItemManualTaskResource;
    }

    @Override
    public List<Cancellation> getCancellations(UUID incidentId, Long providerBookingId) {
        try {
            return cancellationResource.getCancellationsByIncident(new IncidentId(incidentId.toString()));
        } catch (CancellationNotFoundException e) {
            return null;
        }

    }

    @Override
    public ProviderItemManualTask getProviderItemManualTask(UUID providerItemId) {
        try {
            return providerItemManualTaskResource.getProviderItemManualTaskByProviderItemId(new ProviderItemId(providerItemId.toString()));
        } catch (ProviderItemManualTaskNotFoundException e) {
            return null;
        }
    }
}
