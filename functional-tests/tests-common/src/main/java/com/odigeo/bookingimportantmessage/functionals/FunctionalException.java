package com.odigeo.bookingimportantmessage.functionals;

public class FunctionalException extends RuntimeException {
    public FunctionalException(Throwable cause) {
        super(cause);
    }
}
