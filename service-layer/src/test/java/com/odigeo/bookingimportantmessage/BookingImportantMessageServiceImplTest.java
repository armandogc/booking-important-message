package com.odigeo.bookingimportantmessage;

import com.edreamsodigeo.customersupport.cancellation.v2.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v2.model.Item;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.flightincident.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.v1.model.IncidentId;
import com.odigeo.bookingimportantmessage.cancellation.CancellationApiStore;
import com.odigeo.bookingimportantmessage.flightincident.FlightIncidentStore;
import com.odigeo.bookingimportantmessage.travelcompanion.TravelCompanionStore;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class BookingImportantMessageServiceImplTest {

    @Mock
    private FlightIncidentStore flightIncidentStore;

    @Mock
    private CancellationApiStore cancellationApiStore;

    @Mock
    private TravelCompanionStore travelCompanionStore;

    private BookingImportantMessageService bookingImportantMessageService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        bookingImportantMessageService = new BookingImportantMessageServiceImpl(flightIncidentStore, cancellationApiStore, travelCompanionStore);
    }

    @Test
    public void getCancellationTest() {
        when(flightIncidentStore.getIncidents(any(), any())).thenReturn(Collections.singletonList(getIncident()));
        when(cancellationApiStore.getCancellations(any(), any())).thenReturn(Collections.singletonList(getCancellation()));
        when(cancellationApiStore.getProviderItemManualTask(any())).thenReturn(new ProviderItemManualTask());
        when(travelCompanionStore.getBooking(any())).thenReturn(new BookingDetail());
        assertNotNull(bookingImportantMessageService.getMessagesByBookingId(1L));
    }

    private Incident getIncident() {
        Incident incident = new Incident();
        incident.setId(new IncidentId());
        incident.setCancellationId(new CancellationId(UUID.randomUUID().toString()));
        return incident;
    }

    private Cancellation getCancellation() {
        Cancellation cancellation = new Cancellation();
        Item item = new Item();
        ProviderItem providerItem = new ProviderItem();
        providerItem.setId(new ProviderItemId(UUID.randomUUID().toString()));
        item.setProviderItems(Collections.singletonList(providerItem));
        cancellation.setItems(Collections.singletonList(item));
        return cancellation;
    }
}
