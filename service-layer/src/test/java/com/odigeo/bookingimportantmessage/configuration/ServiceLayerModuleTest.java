package com.odigeo.bookingimportantmessage.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import com.odigeo.bookingimportantmessage.BookingImportantMessageService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.AssertJUnit.assertNotNull;

public class ServiceLayerModuleTest {

    @InjectMocks
    private ServiceLayerModule serviceLayerModule;
    @Mock
    private BookingImportantMessageService bookingImportantMessageService;

    @BeforeMethod
    public void setUp() {
        serviceLayerModule = new ServiceLayerModule();
        initMocks(this);
    }

    @Test
    public void dummyTestToCoverJacocoCoverage() {
        assertNotNull(serviceLayerModule);
    }

    @Test
    public void dummyTestToCoverConfigurationMethod() {
        Guice.createInjector(Modules.override(new ServiceLayerModule()).with(new DummyModule()));
    }

    public class DummyModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(BookingImportantMessageService.class).toInstance(bookingImportantMessageService);
        }
    }

}
