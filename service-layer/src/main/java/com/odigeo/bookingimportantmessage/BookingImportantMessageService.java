package com.odigeo.bookingimportantmessage;

public interface BookingImportantMessageService {

    String getMessagesByBookingId(Long bookingId);

}
