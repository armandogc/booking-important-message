package com.odigeo.bookingimportantmessage;

import com.edreamsodigeo.customersupport.cancellation.v2.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v2.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.google.inject.Inject;
import com.google.inject.Singleton;


import com.odigeo.bookingimportantmessage.cancellation.CancellationApiStore;
import com.odigeo.bookingimportantmessage.flightincident.FlightIncidentStore;
import com.odigeo.bookingimportantmessage.travelcompanion.TravelCompanionStore;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Singleton
public class BookingImportantMessageServiceImpl implements BookingImportantMessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingImportantMessageServiceImpl.class);

    private final FlightIncidentStore flightIncidentStore;
    private final CancellationApiStore cancellationApiStore;
    private final TravelCompanionStore travelCompanionStore;

    @Inject
    public BookingImportantMessageServiceImpl(final FlightIncidentStore flightIncidentStore,
                                              final CancellationApiStore cancellationApiStore,
                                              final TravelCompanionStore travelCompanionStore) {
        this.flightIncidentStore = flightIncidentStore;
        this.cancellationApiStore = cancellationApiStore;
        this.travelCompanionStore = travelCompanionStore;
    }

    @Override
    public String getMessagesByBookingId(Long bookingId) {

        List<Incident> incidents = flightIncidentStore.getIncidents(bookingId, null);
        LOGGER.info("Response from FI: " + incidents.toString());
        List<Cancellation> cancellations = cancellationApiStore.getCancellations(incidents.get(0).getId().getId(), null);
        LOGGER.info("Response from CA: " + cancellations.toString());
        ProviderItemManualTask providerItemManualTask = cancellationApiStore.getProviderItemManualTask(cancellations.get(0).getItems().get(0).getProviderItems().get(0).getId().getId());
        LOGGER.info("Response from TT: " + providerItemManualTask.toString());
        BookingDetail bookingDetail = travelCompanionStore.getBooking(bookingId);
        LOGGER.info("Response from TC: " + bookingDetail.toString());

        return "Important message here for booking: " + bookingId + "!";
    }
}
