package com.odigeo.bookingimportantmessage.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.bookingimportantmessage.BookingImportantMessageService;
import com.odigeo.bookingimportantmessage.BookingImportantMessageServiceImpl;

public class ServiceLayerModule extends AbstractModule {

    @Override
    protected void configure() {
        configureJeeServices();
    }

    private void configureJeeServices() {
        bind(BookingImportantMessageService.class).to(BookingImportantMessageServiceImpl.class);
    }
}
